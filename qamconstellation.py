import math
import numpy as np

class QAMConstellation:

    def __init__(self, N_points):
        self.bits_per_const = int(math.log2(N_points))
        self.N_points = N_points
        self.constellation = self.generate_bit_mapping()
        self.constellation_inverse = {c: g for g, c in self.constellation.items()}
        self.average_energy = np.mean(np.abs(list(self.constellation.values()))**2)

        if self.bits_per_const > 8:
            raise Exception("Constellations larger than 256 points not supported")

    def generate_bit_mapping(self):
        side_len = int(math.sqrt(self.N_points))

        side = np.arange((1-side_len), side_len, 2)
        constellation_matrix = np.flip(side[None, :] + 1j * side[:, None], axis=0)

        gray_code = [i^(i>>1) for i in range(self.N_points)]
        constellation_spiral = self.spiral_traverse_ccw(constellation_matrix)

        constellation = {g: c for g, c in zip(gray_code, constellation_spiral)}
        return constellation

    def map(self, data):
        bit_data = np.unpackbits(data)
        # The number of bits needed to make a whole number of constellation symbols
        padding = int((self.bits_per_const - len(bit_data) % self.bits_per_const) % self.bits_per_const)
        bit_data = np.pad(bit_data, (0, padding))
        bit_data = bit_data.reshape((-1, self.bits_per_const))

        # Flip needed along with bit_order="little" to read
        # the binary constellation numbers correctly
        bit_data = np.flip(bit_data, axis=1)
        packed_data = np.packbits(bit_data, axis=1, bitorder="little")
        return np.vectorize(self.constellation.get)(packed_data.flatten())

    def unmap(self, decoded_const_vals):
        binary_ints = np.vectorize(self.constellation_inverse.get)(decoded_const_vals).astype(np.uint8)
        const_bits = np.unpackbits(binary_ints, bitorder='little').reshape(-1, 8)
        binary_data = np.flip(const_bits.reshape(-1, 8)[:, :self.bits_per_const], axis=1)
        return binary_data.flatten()

    def decode_min_dist(self, const_values):
        real_parts = np.zeros_like(const_values)
        im_parts = np.zeros_like(const_values)
        side_len = int(math.sqrt(self.N_points))

        for i in range(3-side_len, side_len - 1, 2):
            # All the middle values
            real_parts[np.logical_and(const_values.real > i-1, const_values.real <= i+1)] = i
        # The end values
        real_parts[const_values.real <= -side_len+2] = 1-side_len
        real_parts[const_values.real > side_len-2] = side_len-1

        for i in range(3-side_len, side_len - 1, 2):
            # All the middle values
            im_parts[np.logical_and(const_values.imag > i-1, const_values.imag < i+1)] = i
        # The end values
        im_parts[const_values.imag <= -side_len+2] = 1-side_len
        im_parts[const_values.imag > side_len-2] = side_len-1

        decoded_const_vals = real_parts + 1j * im_parts
        return self.unmap(decoded_const_vals)

    def get_llrs(self, const_values, var_r, var_i):
        likelihood_ratios = np.zeros(const_values.shape[0] * self.bits_per_const)
        if self.N_points == 4:
            # We use set derived values for QAM4 to speed up computation
            likelihood_ratios[::2] = math.sqrt(self.average_energy) * math.sqrt(2) * const_values.imag/var_i
            likelihood_ratios[1::2] = math.sqrt(self.average_energy) * math.sqrt(2) * const_values.real/var_r
            return likelihood_ratios

        # Get the gray code numbers corresponding to a 1 in each of the bits for each constellation value
        x_values = np.array(list(self.constellation.keys()), dtype=np.uint8)
        # We have to make the list backwards because the smallest bit is in the highest array position
        bit1_x_vals = [x_values[np.bitwise_and(x_values, 2**i) == 2**i] for i in range(self.bits_per_const - 1, -1, -1)]
        bit0_x_vals = [x_values[np.bitwise_and(x_values, 2**i) == 0] for i in range(self.bits_per_const -1, -1, -1)]

        # Get the constellation values corresponding to a 1 in each of the bits
        bit1_const_vals = [np.array([self.constellation[x] for x in x_vals]) for x_vals in bit1_x_vals]
        bit0_const_vals = [np.array([self.constellation[x] for x in x_vals]) for x_vals in bit0_x_vals]

        for i, const in enumerate(const_values):
            for j in range(self.bits_per_const):
                likelihood_ratio = np.sum(self.complex_gauss_exp(const, bit0_const_vals[j], var_r, var_i))/ \
                        np.sum(self.complex_gauss_exp(const, bit1_const_vals[j], var_r, var_i))

                likelihood_ratios[i*self.bits_per_const + j] = likelihood_ratio

        return np.log(likelihood_ratios)

    def get_random_values(self, size):
        return np.random.choice(list(self.constellation.values()), size)

    @staticmethod
    def complex_gauss_exp(y, mu, var_r, var_i):
        # Calculate a (multivariate) gaussian exponent
        return np.exp(-((y.real - mu.real)**2)/(2 * var_r)
                      -((y.imag - mu.imag)**2)/(2 * var_i))

    @staticmethod
    def spiral_traverse_ccw(A):
        # we assign gray code values in a spiral around the constellation
        A = np.array(A)
        out = []
        while(A.size):
            out.append(np.flip(A[0]))    # take first row
            A = A[1:].T[::]   # cut off first row and rotate counterclockwise
        return np.concatenate(out)
