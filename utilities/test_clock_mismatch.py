"""
We try to measure the clock mismatch between transmitter and receiver
By transmitting a long sine wave then multiplying the transmitted and received
signals together. This should produce a sum frequency (2f, which we filter out)
and a difference frequency (the clock mismatch) which can be found with an FFT.

Noise seems to make this more difficult inpractice as lots of FFT peaks appear.
"""


import numpy as np
import scipy.signal

from constants import *
from scipy.io import wavfile
import matplotlib.pyplot as plt
import scipy.signal as sig


# Generate a long sine wave
time = 120
sine_period = 50 # The number of samples of the period
'''
# Uncomment to write the sine wave output to file
t = np.linspace(0, (time * Fs/sine_period) * 2* np.pi, time * Fs)

sin_wave = (np.sin(t) * 32767).astype(dtype=np.int16)
wavfile.write("sine_wave.wav", Fs, sin_wave)
'''

Fs, sin_wave = wavfile.read("sine_wave.wav")
_, sin_recv = wavfile.read("sine_rec.wav")

# Now multiply the two together to get an envelope
product = sin_wave * sin_recv

# Low pass and downsample by 100
numerator, denominator = sig.cheby1(4, 1, 0.2/sine_period)
product_low_passed = sig.lfilter(numerator, denominator, product)[::100]

fft = np.fft.rfft(product_low_passed)

plt.plot(np.abs(fft))
plt.show()
