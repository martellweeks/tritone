import numpy as np
from ofdm_modulate import get_block_len
from constants import *
import matplotlib.pyplot as plt

transmit_file = "../source_files/Li_Sao_half.txt"
receive_file = "../decoded_files/Li_Sao.txt"
header_len = 0  # Only used if the header was written into the file


def count_byte_errors(data1, data2):
    byte_errors = 0
    for byte1, byte2 in zip(data1, data2):
        if byte1 != byte2:
            byte_errors += 1

    return byte_errors


def count_bit_errors(data1, data2):
    bit_errors = 0
    for byte1, byte2 in zip(data1, data2):
        errors = byte1 ^ byte2  # xor
        bit_errors += bin(errors).count("1")

    return bit_errors


def count_bit_errors_with_bits(bits1, bits2):
    return np.count_nonzero(np.array([b1 != b2 for b1, b2 in zip(bits1, bits2)]))


def plot_byte_bit_errors_per_symbol(data1, data2, block_len):
    bits_per_block = constellation.bits_per_const * block_len
    num_blocks = min((len(data1)*8)//bits_per_block, (len(data2)*8)//bits_per_block)

    bytes_per_block = bits_per_block/8
    print(bits_per_block)
    print(bytes_per_block)

    # Note we only roughly handle the padding of bits to make constellation symbols
    # And the splitting of bytes to make bits for byte error
    bit_es = []
    byte_es = []

    bits1 = np.unpackbits(np.frombuffer(data1, dtype=np.uint8))
    bits2 = np.unpackbits(np.frombuffer(data2, dtype=np.uint8))

    for i in range(num_blocks):
        bit_es.append(count_bit_errors_with_bits(bits1[bits_per_block * i: bits_per_block * (i+1)],
                                       bits2[bits_per_block * i: bits_per_block * (i+1)]))

        byte_es.append(count_byte_errors(data1[int(bytes_per_block * i): int(bytes_per_block * (i+1))],
                                       data2[int(bytes_per_block * i): int(bytes_per_block * (i+1))]))

    plt.plot(np.array(bit_es)/bits_per_block, label="bit errors")
    plt.plot(np.array(byte_es)/bytes_per_block, label="byte_errors")
    plt.legend()
    plt.title("Error fractions per ofdm symbol")
    plt.show()


def plot_bit_errors_per_subcarrier(data1, data2, block_len):
    bits_per_block = constellation.bits_per_const * block_len
    num_blocks = min((len(data1)*8)//bits_per_block, (len(data2)*8)//bits_per_block)

    bits_per_subcarrier = num_blocks * constellation.bits_per_const

    bits1 = np.unpackbits(np.frombuffer(data1, dtype=np.uint8))
    bits2 = np.unpackbits(np.frombuffer(data2, dtype=np.uint8))

    reshaped_bits1 = bits1.reshape(-1, constellation.bits_per_const)
    reshaped_bits2 = bits2.reshape(-1, constellation.bits_per_const)

    bit_errors = []

    for i in range(block_len):
        bit_errors.append(count_bit_errors_with_bits(
            reshaped_bits1[i::bits_per_block].flatten(),
            reshaped_bits2[i::bits_per_block].flatten()))


    plt.plot(np.array(bit_errors)/bits_per_subcarrier)
    plt.legend()
    plt.title("bit errors per subcarrier")
    plt.show()


with open(transmit_file, 'rb') as t, open(receive_file, 'rb') as r:
    transmitted = t.read()
    received = r.read()

    if len(transmitted) != len(received):
        print("Lengths of transmitted and received files differ")
        print(f"transmitted length:{len(transmitted)}  "
              f", received length:{len(received)}")

        # Remove any header and extra at the end to make the same length
        received = received[header_len:header_len+len(transmitted)]

    print(f"File length: {len(received)} bytes")

    byte_e = count_byte_errors(transmitted, received)
    print(f"Byte errors: {byte_e}  (fraction {byte_e/len(transmitted)})")

    bit_e = count_bit_errors(transmitted, received)
    print(f"Bit errors: {bit_e}  (fraction {bit_e/(len(transmitted) * 8)})")

    block_len = get_block_len(N, (min_subcarrier_freq, max_subcarrier_freq))

    plot_byte_bit_errors_per_symbol(transmitted, received, block_len)
    plot_bit_errors_per_subcarrier(transmitted, received, block_len)
