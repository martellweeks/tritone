import numpy as np
from scipy.io import wavfile
import scipy.signal as sig
import matplotlib.pyplot as plt
from constants import *

def test_awgn_channel(data, snr, random_delay):
    receive_data = np.pad(data, (random_delay, 0))

    # Estimate noise power to get snr dB
    noise_std_dev = np.sqrt(np.sum(data ** 2)/(10 ** snr/10))
    receive_data += np.random.randn(receive_data.shape[0]) * noise_std_dev
    return receive_data


def test_impulse_channel(data, snr, random_delay, impulse_response):
    channel_data = sig.convolve(data, impulse_response)
    return test_awgn_channel(channel_data, snr, random_delay)


def int16_to_float64(data):
    # Normalise to floating point data in [-1, 1]
    int16max = 32767
    return data.astype(np.float64)/int16max


Fs_data, data = wavfile.read('transmit_files/Li_sao_new_transmitter.wav')
assert Fs==Fs_data, "Sample rate unmatched"
data = int16_to_float64(data)

# Define the channel
test_channel = np.real(np.load(
    "channel_data/DansRoomChannelEstimate.npy")).astype(np.float64)
test_channel /= np.sqrt(np.sum(test_channel**2))

random_delay = 0 # 9504 # np.random.randint(0, Fs)
resample_shift = 1


channel_data = test_impulse_channel(data, 10, random_delay, test_channel)
channel_data = sig.resample(channel_data, channel_data.shape[0] + resample_shift)
if show_plots:
    plt.plot(channel_data)
    plt.xlabel("Samples")
    plt.title("Channel output data")
    plt.show()

wavfile.write('received_data/Li_Sao_new_ldpc_ch.wav', Fs, channel_data)
print("Complete")