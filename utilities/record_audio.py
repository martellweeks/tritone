import sounddevice as sd
from scipy.io import wavfile
from os.path import join
import numpy as np
from constants import *


def float_to_int16(signal):
    abs_max = max(signal.max(), -signal.min())
    int16max = 32767
    return ((signal/abs_max) * int16max).astype(np.int16)

duration = float(input("Enter a record duration (s) and press enter to start recording"))

recording = sd.rec(int(duration * Fs), samplerate=Fs, channels=1)
sd.wait()

recording_int = float_to_int16(recording)

filename = input("Enter a file path to save the recording")
wavfile.write(filename, Fs, recording_int)
