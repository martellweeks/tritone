import sounddevice as sd
import matplotlib.pyplot as plt
from scipy.io import wavfile
from constants import *

duration = 20 # Duration of recording (in seconds)
recording = sd.rec(
duration * Fs, samplerate=Fs, channels=1, blocking=True
).flatten()

wavfile.write('./received_data/recording.wav', Fs, recording)
