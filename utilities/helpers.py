import numpy as np


def float_to_int16_range(signal):
    """Performs normalisation and scaling to int 16
     range wtihout dtype casting"""
    abs_max = max(signal.max(), -signal.min())
    int16max = 32767
    return (signal / abs_max) * int16max


def norm_to_float64(signal):
    abs_max = max(signal.max(), -signal.min())
    return signal.astype(np.float64) * abs_max


def float_to_int16(signal):
    return float_to_int16_range(signal).astype(np.int16)


def int16_to_float64(data):
    # Normalise to floating point known_ofdm_symbols in [-1, 1]
    int16max = 32767
    return data.astype(np.float64)/int16max
