import numpy as np
import matplotlib.pyplot as plt
from constants import *
import scipy.optimize as optim
import math


def ldpc_encode(data):
    K = ldpc_module.K
    data = np.unpackbits(data)
    random_padding = np.random.choice([0,1], K - (len(data) % K)) # Add random padding to match dimension
    data = np.concatenate([data, random_padding])
    data_blocks = [data[i*K: (i+1)*K] for i in range(0, int(len(data)/K))]
    encoded_data = [ldpc_module.encode(data_blocks[i]) for i in range(len(data_blocks))]
    for i in range(len(encoded_data)):
        assert np.count_nonzero(np.mod(np.matmul(encoded_data[i],np.transpose(ldpc_module.pcmat())), 2)) == 0
        assert data_blocks[i].all() == encoded_data[i][:int(len(encoded_data[i])*ldpc_rate)].all()
    encoded_data = [np.packbits(sublist) for sublist in encoded_data]
    return bytearray([item for sublist in encoded_data for item in sublist])


def get_blocks(const_values):
    # We pad the final block with random constellation values
    random_padding = np.random.choice(list(constellation.constellation.values()),
                                      block_len - (len(const_values) % block_len))
    const_vals_padded = np.concatenate([const_values, random_padding])

    return [const_vals_padded[i*block_len: (i+1)*block_len]
            for i in range(0, int(len(const_vals_padded)/block_len))]


def get_block_len(N, freq_range):
    return math.ceil(freq_range[1] * N / Fs)\
           - math.ceil(freq_range[0] * N / Fs)


def assign_subcarriers(blocks):
    X_list = []
    for block in blocks:
        # Pad with random constellation values
        random_padding = constellation.get_random_values(int(N//2) - 1)
        X = np.concatenate([np.zeros(1), random_padding, np.zeros(1),
                            np.flip(np.conj(random_padding))])

        X[min_freq_bin:max_freq_bin] = block
        X[N-min_freq_bin:N-max_freq_bin:-1] = np.conj(block)
        X[0] = 0
        X[N//2] = 0

        X_list.append(X)

    return X_list


def generate_symbols(X_list):
    symbols = []
    for X in X_list:
        x = np.real(np.fft.ifft(X))
        # Add cyclic prefix
        symbol = np.pad(x, (L, 0), mode="wrap")
        symbols.append(symbol)

    return symbols


def calculate_papr(signal, original_signal_power):
    square_max = max(signal.max()**2, signal.min()**2)
    return 10 * math.log10(square_max / original_signal_power)


def reduce_papr(X_list):
    clip_mag_divider = 2
    papr_reduced_X_list = []

    for X in X_list:
        x = np.real(np.fft.ifft(X))
        original_signal_power = constellation.average_energy * 2*block_len/N

        # Clip and FFT
        clip_mag = max(x.max(), -x.min())/clip_mag_divider
        x_clip = np.clip(x, -clip_mag, clip_mag)
        X_clip = np.fft.fft(x_clip)

        X_clip[min_freq_bin:max_freq_bin] = 0
        X_clip[N-min_freq_bin:N-max_freq_bin:-1] = 0

        # Enforce no zero offset
        X_clip[0] = 0
        if N % 2 == 0:
            X_clip[N//2] = 0

        def scaled_value_optim(scale):
            X_clip_scaled = X_clip * scale

            # Assign the used frequency bins back to the original signal
            X_clip_scaled[min_freq_bin:max_freq_bin] = X[min_freq_bin:max_freq_bin]
            X_clip_scaled[N-min_freq_bin:N-max_freq_bin:-1] = X[N-min_freq_bin:N-max_freq_bin:-1]
            x = np.real(np.fft.ifft(X_clip_scaled))
            return calculate_papr(x, original_signal_power)

        scale = 1
        result = optim.minimize(scaled_value_optim, scale, bounds=[(0.5, 1.5)])

        X_best = X_clip * result.x

        # Assign the used frequency bins back to the original signal
        X_best[min_freq_bin:max_freq_bin] = X[min_freq_bin:max_freq_bin]
        X_best[N-min_freq_bin:N-max_freq_bin:-1] = X[N-min_freq_bin:N-max_freq_bin:-1]
        papr_reduced_X_list.append(X_best)

        if debug_plots:
            plt.plot(x, label="Original")
            plt.plot(np.fft.ifft(X_best).real, label="Peak reduced")
            plt.xlabel("Samples")
            plt.ylabel("Amplitude")
            plt.legend()
            plt.title("Peak reduction")
            plt.show()

    return papr_reduced_X_list


def normalise_block_power_out_of_band(X_list):
    X_array = np.array(X_list)
    X_powers = np.mean(np.abs(X_array)**2, axis=-1)
    out_band_noise_power = X_powers.max() - X_powers
    noise_amplitudes = ((N - 2 * block_len)/N) * np.sqrt(out_band_noise_power)

    random_padding = constellation.get_random_values(int(N//2) - 1)
    complex_noise = np.concatenate([np.zeros(1), random_padding, np.zeros(1), np.flip(np.conj(random_padding))])
    complex_noise[min_freq_bin:max_freq_bin] = 0
    complex_noise[N-max_freq_bin:N-min_freq_bin] = 0

    # Add the right amount of noise out of band to normalise the power of all blocks
    X_normalised_array = X_array + complex_noise[None, :] * noise_amplitudes[:, None]

    return list(X_normalised_array)


def modulate_known_ofdm_symbol():
    known_X = np.load(known_OFDM_symbol_path) * constellation.average_energy
    return generate_symbols(list([known_X]))[0]


def ofdm(data, encode_ldpc):
    if encode_ldpc:
        data = ldpc_encode(data)
    const_vals = constellation.map(data)
    blocks = get_blocks(const_vals)
    X_list = assign_subcarriers(blocks)
    papr_reduced_X_list = reduce_papr(X_list)
    norm_X_list = normalise_block_power_out_of_band(papr_reduced_X_list)
    symbols = generate_symbols(norm_X_list)

    return symbols




