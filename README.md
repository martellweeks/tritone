# TriTone - A Baseband OFDM Based Audio Transmission System

## System Overview

The overall structure of the transmitter and receiver modules can be seen below. 

![Flow diagram](assets/SystemOverview.png)

## Transmission results

Transmitted (left) and received (right) images
(transmission image from https://lithub.com/the-25-most-iconic-book-covers-in-history/).

![Transmitted image](assets/Catch-22-transmitted.png)
![Received image](assets/Catch-22-received.png)



Sections of received text from a transmission consisting of https://en.wikipedia.org/wiki/Rick_Astley and the lyrics to
Rick Astley's Never Gonna Give You Up

> Lyrics  
> We're no strangers to love  
> You know the rules and so do I (do I)
>
> ...
>
> students often rickrolled their classmates  
> and teachers.[35] A 4K remaster of the  
> "Never Gonna Give You Up" music video 7\`æte  
> ^;rah énepr$yè26s1/O³6õc£6]"n'NTe dg(!nv D  
> hA"qokÃ©mgn Aomxiny"had"angouoced*1"nu|y :021
>
> ...
>
> Text is available under th% creáôive Commons  
> Atvribqtuoo-S,\`veAlike License 3.0; addhtiona,  
> terms may appìy&

## Usage

### Dependencies

Python libraries:
 - Numpy
 - Scipy
 - Matplotlib
 - Sounddevice (only for recording)

For LDPC coding operation a C library must be compiled - see instructions for this in the LDPC [readme](ldpc_jossy/readme.md).

### Running the transmitter and receiver

To generate a transmission from a source file, edit the input and output filepath variables in `transmitter.py` then run.

Play the generated .wav file and record the transmission on another device. Alternatively, `utilities/test_channel.py` 
provides a simulated channel with configurable SNR, impulse response, and clock frequency mismatch.

To decode the transmission, edit the input filepath variable in `receiver.py` then run. If demodulation and decoding are
successful the output file will be written to the folder `decoded_files`.

## Options

Constants used in for transission and receival are set in `constants.py`. Current values are set according to the
[Catch-22 standard](https://www.overleaf.com/project/628befa0a5f784cb3d188f72). The options for `show_plots` 
and `debug_plots` enable and disable output of various plots, which can be used for debugging.

## Jupyter notebooks

Jupyter notebooks containing test code for different methods/algorithms before integration into the main code are available
in the folder `notebooks`