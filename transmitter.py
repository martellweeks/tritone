from scipy.io import wavfile
import numpy as np
import ofdm_modulate
from scipy.signal import chirp
import matplotlib.pyplot as plt
from constants import *
from utilities.helpers import float_to_int16, float_to_int16_range
import math

# File to read
in_filepath = "source_files/Li_Sao.txt"
out_filepath = "transmit_files/Li_sao_new_transmitter.wav"

encode_ldpc = True


def generate_chirp(chirp_length):
    amplitude = 30000
    t = np.linspace(0, chirp_length, chirp_samp)
    x1 = chirp(t, min_subcarrier_freq, chirp_length, max_subcarrier_freq, method='linear')
    return x1 * amplitude


def generate_white_noise(white_noise_length):
    sigma = 5000
    x2 = np.random.randn(Fs * white_noise_length) * sigma
    return x2


def silence(silence_length):
    return np.zeros(Fs * silence_length)


def append_data_header(data, filename):
    data_len = len(data)
    header = data_len.to_bytes(4, 'little') + filename.encode('ascii') + b'\0'
    print(f"Header length: {len(header)} bytes")
    return bytearray(header + data)


def make_frames(data, chirp, known_symbol):
    num_frames = math.ceil(len(data)/max_frame_len)
    frame_data = [data[i*max_frame_len:(i+1)*max_frame_len] for i in range(num_frames)]
    frames = []

    for f_data in frame_data:
        frame = np.concatenate([chirp, known_symbol, *f_data, known_symbol, chirp])
        frames.append(frame)

    return frames


def normalise_symbols_and_known_symbol_to_int16_range(symbols, known_OFDM):
    int16max = 32767
    max_value = max([s.max() for s in symbols] + [-s.min() for s in symbols] + [known_OFDM.max(),])
    return [s * int16max/max_value for s in symbols], known_OFDM * int16max/max_value


def get_final_average_signal_power(symbols):
    symbol_array = np.array(symbols)
    symbol_array = symbol_array[:, L:]  # Remove cyclic prefix
    symbol_fft = np.fft.fft(symbol_array)
    average_symbol_energy = np.mean(np.abs(symbol_fft[:, min_freq_bin:max_freq_bin]) ** 2)
    return average_symbol_energy

with open(in_filepath, 'rb') as f:
    print("Reading known_ofdm_symbols ...")
    data = bytearray(f.read())
    data = append_data_header(data, in_filepath.split("/")[-1])

    print("Performing modulation ...")
    symbols = ofdm_modulate.ofdm(data, encode_ldpc)

    known_symbol_single = ofdm_modulate.modulate_known_ofdm_symbol()
    known_symbol = np.concatenate((known_symbol_single[:L],
                                   np.tile(known_symbol_single[L:], known_OFDM_no)))

    symbols_normalised, known_symbol = normalise_symbols_and_known_symbol_to_int16_range(symbols, known_symbol)
    print(f"Average signal energy : {get_final_average_signal_power(symbols_normalised)}")

    print("Generating frames ...")

    init = silence(silence_len)
    x1 = generate_chirp(chirp_len)
    sync_preamble = [init, x1]
    sync_endamble = [x1, init]

    frames = make_frames(symbols_normalised, x1, known_symbol)

    signal_final = np.concatenate(sync_preamble + frames + sync_endamble)

    if show_plots:
        plt.plot(signal_final)
        plt.xlabel("Samples")
        plt.title("Output signal")
        plt.show()

    print("Writing output ...")
    wavfile.write(out_filepath, Fs, float_to_int16(signal_final))

    print(f"total signal length: {len(signal_final)} samples ({len(signal_final) / Fs} s)")
    print("Complete")
