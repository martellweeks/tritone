import numpy as np
import math
import scipy.signal as sig
from constants import *
from scipy.io import wavfile
import matplotlib.pyplot as plt
import scipy.optimize as optim


def convolve_chirp_rev(data):
    t = np.linspace(0, chirp_len, chirp_samp)
    chirp_rev = np.flip(sig.chirp(t, min_subcarrier_freq, chirp_len, max_subcarrier_freq))
    return sig.convolve(data, chirp_rev)


def chirp_synchronise(chirp_conv, backshift = False):
    chirp_index = []
    counting = True
    max_amp = 0

    while counting:
        chirp_index.append(chirp_sync_backshift(chirp_conv, np.argmax(chirp_conv)) + 1 if backshift else np.argmax(chirp_conv) + 1)
        if max_amp == 0:
            max_amp = np.amax(chirp_conv)
        chirp_conv[int(np.argmax(chirp_conv)-chirp_samp/10):int(np.argmax(chirp_conv)+chirp_samp/10)] = 0
        if len(chirp_index) % 2 == 0 and np.amax(chirp_conv) < 0.5 * max_amp:
            counting = False
    chirp_index.sort()
    frame_index = np.array(chirp_index[::2])
    return frame_index


def chirp_end_resync(chirp_conv, backshift = True):
    end_chirp = np.argmax(chirp_conv[int(len(chirp_conv)/2):]) + int(len(chirp_conv)/2) - chirp_samp + 1
    if backshift:
        end_chirp = chirp_sync_backshift(chirp_conv, end_chirp)
    return int(end_chirp)


def chirp_sync_backshift(chirp_conv, start_chirp):
    # Function that detects the actual start of the signal by
    # tracking the growing gain of impulse response
    start_peak = start_chirp + chirp_samp - 1
    for i in range(100):
        if np.amax(np.abs(chirp_conv[start_peak-i-10:start_peak-i])) <= chirp_conv[start_peak]*0.1:
            start_chirp -= (i-1)
            break
    return start_chirp


def chirp_estimate(data, chirp_conv, start_chirp, second_chirp):
    '''Takes FFT of recorded chirp, divide by FFT of ideal chirp
    then IFFT to give impulse response. Averaged between two chirps.'''
    t = np.linspace(0, chirp_len, chirp_samp)
    window = np.hamming(chirp_samp)
    window_L = np.hamming(L)
    chirp_freq = np.fft.fft(sig.chirp(t, min_subcarrier_freq, chirp_len, max_subcarrier_freq), N)
    sig_freq = np.fft.fft(window * data[start_chirp:start_chirp+chirp_samp], N)
    chirp_resp = sig_freq / chirp_freq
    sig_freq_second = np.fft.fft(window * data[second_chirp:second_chirp+chirp_samp], N)
    chirp_resp += sig_freq_second / chirp_freq
    chirp_resp = chirp_resp / 2
    return np.real(np.fft.ifft(chirp_resp))


def chirp_estimate_conv(data, chirp_conv, start_chirp, second_chirp):
    '''Directly takes fragment from convolved signal as an impulse
    response. Averaged between two chirps.'''
    start_peak = start_chirp + chirp_samp - 1
    second_peak = second_chirp + chirp_samp - 1
    impulse_resp = chirp_conv[start_peak:start_peak+N]
    impulse_resp += chirp_conv[second_peak:second_peak+N]
    return impulse_resp / 2


def white_noise_sync_estimate(data, chirp_conv, noise_path, Fs, chirp_len, silence_len, white_noise_len, L):
    noise = wavfile.read(noise_path)[1]    
    start_chirp, second_chirp = chirp_synchronise(chirp_conv)
    start_noise = start_chirp+Fs*(2*chirp_len+silence_len)
    noise_clip = data[start_noise:start_noise+white_noise_len*Fs]
    f, Sxy = sig.csd(noise, noise_clip, fs = Fs, nperseg = 1022, noverlap = 1020)
    time_resp = np.fft.ifft(Sxy)
    peak = np.argmax(abs(time_resp))
    if L/2-peak < peak:
        time_shift = (int(L/2)-peak)*2
    else:
        time_shift = -peak*2
    noise_clip = data[start_noise-time_shift:start_noise+white_noise_len*Fs-time_shift]
    f, Sxy_new = sig.csd(noise, noise_clip, fs = Fs, nperseg = 1022, noverlap = 1020)
    freq_resp = np.zeros(1024, dtype=np.complex128)
    freq_resp[:512] = Sxy_new
    freq_resp[:511:-1] = np.conj(Sxy_new)
    return freq_resp, start_chirp-time_shift


def estimate_constellation_magnitude(block):
    average_received_energy = np.mean(block * np.conj(block))
    return math.sqrt(average_received_energy/constellation.average_energy)


def estimate_channel_known_ofdm(data, initial_sync):
    known_block_fft = np.load(known_OFDM_symbol_path) * math.sqrt(constellation.average_energy)
    known_block_fft = known_block_fft[min_freq_bin: max_freq_bin]

    block_start = initial_sync + chirp_samp
    data_end = len(data) - L - known_OFDM_no*N - chirp_samp
    symbols_front = data[block_start + L:block_start + known_OFDM_no*N + L].reshape(known_OFDM_no, N)
    symbols_end = data[data_end + L:data_end + L + known_OFDM_no*N].reshape(known_OFDM_no, N)
    # symbols = np.concatenate([symbols_front, symbols_end])

    symbol_ffts_front = np.fft.fft(symbols_front)[:, min_freq_bin:max_freq_bin]
    symbol_ffts_end = np.fft.fft(symbols_end)[:, min_freq_bin:max_freq_bin]

    channel_freq_response_front = np.zeros(N, dtype=np.complex128)
    channel_freq_response_front[min_freq_bin: max_freq_bin] = np.mean(symbol_ffts_front/known_block_fft[None, :], axis=0)
    channel_freq_response_front[N-min_freq_bin:N-max_freq_bin:-1] = np.conj(channel_freq_response_front[min_freq_bin: max_freq_bin])

    channel_freq_response_end = np.zeros(N, dtype=np.complex128)
    channel_freq_response_end[min_freq_bin: max_freq_bin] = np.mean(symbol_ffts_end/known_block_fft[None, :], axis=0)
    channel_freq_response_end[N-min_freq_bin:N-max_freq_bin:-1] = np.conj(channel_freq_response_end[min_freq_bin: max_freq_bin])

    noise_front = symbol_ffts_front/channel_freq_response_front[None, min_freq_bin:max_freq_bin] - \
            known_block_fft[None, :]
    noise_end = symbol_ffts_end/channel_freq_response_end[None, min_freq_bin:max_freq_bin] - \
                  known_block_fft[None, :]

    noise = (noise_end + noise_front)/2
    channel_freq_response = (channel_freq_response_front + channel_freq_response_end)/2

    if debug_plots:
        plt.plot(np.abs(channel_freq_response_front), label="preamble estimate")
        plt.plot(np.abs(channel_freq_response_end), label="end-amble estimate")
        plt.xlabel("Frequency bin")
        plt.ylabel("Absolute magnitude")
        plt.title("Channel frequency response")
        plt.legend()
        plt.tight_layout()
        plt.show()

        plt.plot(np.angle(channel_freq_response_front), label="preamble estimate")
        plt.plot(np.angle(channel_freq_response_end), label="end-amble estimate")
        plt.xlabel("Frequency bin")
        plt.ylabel("Phase angle (rad)")
        plt.title("Channel frequency response")
        plt.legend()
        plt.tight_layout()
        plt.show()

        plt.plot(np.real(np.fft.ifft(channel_freq_response)))
        plt.xlabel("Samples")
        plt.ylabel("Amplitude")
        plt.title("Channel impulse response (average)")
        plt.tight_layout()
        plt.show()

    real_var_estimate = np.mean(noise.real**2)
    imag_var_estimate = np.mean(noise.imag**2)

    return (channel_freq_response_front, channel_freq_response_end), \
           (real_var_estimate, imag_var_estimate), initial_sync


# Currently unused functions below

def estimate_sync_offset(freq_response):
    low_lim_ind = math.ceil(min_subcarrier_freq * N / Fs)
    high_lim_ind = math.ceil(max_subcarrier_freq * N / Fs)

    phase = np.unwrap(np.angle(freq_response))
    phase_offset = ((phase[high_lim_ind-1] - phase[low_lim_ind])
                   + (phase[N-high_lim_ind+1] - phase[low_lim_ind]))\

    time_offset = phase_offset * Fs/(2 * high_lim_ind - low_lim_ind * 2 * np.pi)
    return time_offset


def linear_regress_phase(frequency_response):
    low_lim_ind = math.ceil(min_subcarrier_freq * N / Fs)
    high_lim_ind = math.ceil(max_subcarrier_freq * N / Fs)

    phase = np.unwrap(np.angle(frequency_response))
    valid_phases = np.concatenate([phase[low_lim_ind: high_lim_ind],
                                   phase[N-high_lim_ind: N-low_lim_ind]])

    freq_ind = np.concatenate([np.arange(low_lim_ind, high_lim_ind, 1),
                               np.arange(N-high_lim_ind, N-low_lim_ind)])

    f = lambda x, a: a * x
    lin_coeff, _ = optim.curve_fit(f, freq_ind, valid_phases, 0)

    if debug_plots:
        plt.plot(freq_ind, valid_phases)
        plt.plot(np.unwrap(np.angle(frequency_response)))
        plt.plot(freq_ind, lin_coeff * freq_ind)
        plt.show()

    return lin_coeff[0]


def detect_time_domain_onset(signal):
    # Find the start of the channel impulse response in the time domain
    window_size = 20
    offset = 5
    prefixed_signal = np.pad(signal, (window_size, 0), mode='wrap')
    conv = np.convolve(prefixed_signal ** 2, np.ones(window_size)/window_size, mode="valid")

    total_energy = np.mean(conv)

    plt.plot(conv[offset:]/conv[:-offset])
    plt.show()

    thresh = 0.01
    plt.plot(conv/total_energy)
    plt.show()










