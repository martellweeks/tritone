from constants import *
import numpy as np


def ldpc_encode(data):
    K = ldpc_module.K
    data = np.unpackbits(data)
    print(data)
    random_padding = np.random.choice([0,1], K - (len(data) % K)) # Add random padding to match dimension
    print(random_padding.shape)
    data = np.concatenate([data, random_padding])
    data_blocks = [data[i*K: (i+1)*K] for i in range(0, int(len(data)/K))]
    print(data_blocks)
    encoded_data = [ldpc_module.encode(data_blocks[i]) for i in range(len(data_blocks))]
    assert np.mean(np.mod(np.matmul(encoded_data[0],np.transpose(ldpc_module.pcmat())), 2)) == 0
    encoded_data = [np.packbits(sublist) for sublist in encoded_data]
    return bytearray([item for sublist in encoded_data for item in sublist])


def ldpc_decode(llrs):
    code_len = ldpc_module.N
    num_code_blocks = llrs.shape[0]//code_len
    post_llrs = []

    for i in range(num_code_blocks):
        block_llr = llrs[i*code_len:(i+1)*code_len]
        post_llr, it, = ldpc_module.decode(block_llr, ldpc_decode_mode)
        print(f"Code block: {i}, LDPC iterations used : {it}\r", end='')
        if it == 200:
            print("\nWarning: LDPC decode reached max iterations")
        post_llrs.append(post_llr[:int(len(post_llr)*ldpc_rate)])

    return np.concatenate(post_llrs)


with open("../source_files/Li_Sao.txt", 'rb') as f:
    data = bytearray(f.read())[:96]
    print(data)

    ldpc_encoded_bytes = ldpc_encode(data)
    print(ldpc_encoded_bytes)

    encoded_bits = np.unpackbits(ldpc_encoded_bytes)
    llrs = 10 * (0.5 - encoded_bits)

    decoded_data = ldpc_decode(llrs)
    decoded_bits = (decoded_data <= 0).astype(np.uint8)
    print(decoded_bits[:])


    file_data = np.packbits(decoded_bits, bitorder="big")
    file_bytearray = bytearray(file_data)

    print(file_bytearray)