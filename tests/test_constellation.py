from qamconstellation import QAMConstellation
import numpy as np

constellation_sizes = [4, 16, 64, 256]

for c_size in constellation_sizes:
    print(f"Testing mapping and demaping for size {c_size}")
    const = QAMConstellation(c_size)
    bytes = np.random.randint(0, 255, 100, dtype=np.uint8)
    const_values = const.map(bytes)
    decoded_bits = const.decode_min_dist(const_values)

    padding = len(decoded_bits) % 8
    if padding != 0:
        decoded_bits = decoded_bits[:-padding]
    decoded_bytes = bytearray(np.packbits(decoded_bits, bitorder='big'))

    assert np.all(decoded_bytes == bytes), f"Encoded and decoded bytes dont match for constellation size {c_size}"

    print(f"Testing soft demapping for size {c_size}")
    LLR = const.get_llrs(const_values, 0.5, 0.5)
    decoded_bits_2 = (LLR <= 0).astype(np.uint8)

    padding = len(decoded_bits) % 8
    if padding != 0:
        decoded_bits_2 = decoded_bits_2[:-padding]
    decoded_bytes_2 = bytearray(np.packbits(decoded_bits_2, bitorder='big'))

    assert np.all(decoded_bytes_2 == bytes), f"Decoded version of soft demapping doesnt match encoded data for constellation size {c_size}"

print("All passed!")