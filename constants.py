'''File that defines global constants
to be used throughout the modem
either set by standards or by team'''
from qamconstellation import QAMConstellation
from ldpc_jossy.py import ldpc
import matplotlib.pyplot as plt

show_plots = False   # Enable plotting of a few things
debug_plots = False  # Enable further debug plotting

# Plot styling parameters
plt.rcParams["figure.figsize"] = (7,5)
plt.rcParams['axes.spines.right'] = False
plt.rcParams['axes.spines.top'] = False

# Constants for OFDM

L = 512         # Cyclic prefix length / channel response length
N = 4096        # Data length per block
B = L + N       # Block length
Fs = 48000      # Sampling rate
Fs = Fs # Sampling rate

min_subcarrier_freq = 1000   # Minimum subcarrier frequency, Hz
max_subcarrier_freq = 10000  # Maximum subcarrier frequency, Hz

min_freq_bin = 86           # This bin included
max_freq_bin = 853 + 1      # This bin excluded (853 max)
block_len = max_freq_bin - min_freq_bin

# Constellation definition
qam_size = 4
constellation = QAMConstellation(qam_size)

# LDPC object definition
ldpc_module = ldpc.code(standard='802.16', rate='1/2', z=64, ptype='A')
ldpc_rate = 0.5
ldpc_decode_mode = "sumprod2"

# Constants for synchronisation and estimation

chirp_len = 1              # Length of single chirp, sec
chirp_samp = chirp_len * Fs # Length of single chirp in known_ofdm_symbols samples
chirp_no = 2                # Number of concatenated chirps, currently double
silence_len = 1             # Length of silent block, sec
total_sync_block_len = chirp_len*chirp_no # Length AFTER START OF FIRST CHIRP, sec

known_OFDM_no = 4         # Number of known OFDM symbols at the beginning and end of a frame
known_OFDM_sample = known_OFDM_no * N + L  # Length of known OFDM symbols in samples

known_OFDM_symbol_path = "known_ofdm_symbols/known_ofdm_symbol_3_6.npy"

# Frame constants
max_frame_len = 128
