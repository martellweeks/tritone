import ldpc
import numpy as np

c = ldpc.code()

u = np.random.randint(0, 2, c.K)
# print(f"Input {u}")

x = c.encode(u)
print(f"Encoded {x}")

print(f"The product of information vector and parity "
       f"check matrix {np.mod(np.matmul(x, np.transpose(c.pcmat())), 2)}")

y = 10*(.5-x)
app, it = c.decode(y, 'sumprod')

print(app)
print(it)

# np.nonzero((app<0) != x)
