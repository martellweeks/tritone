import numpy as np
import math
import matplotlib.pyplot as plt
from scipy.io import wavfile
import scipy.signal as sig
import synchronise_estimate as syncest
from constants import *
from utilities.helpers import int16_to_float64, norm_to_float64

data_has_header = True
method = "known_ofdm"

coded_with_ldpc = True

# Import wavfile
recorded_filepath = "received_data/catch22_ldpc_dpo.wav"
dtype = recorded_filepath.split('.')[-1]
if dtype == "wav":
    Fs_data, rawdata = wavfile.read(recorded_filepath)
    assert Fs==Fs_data, "Sampling rate unmatched"
    data = int16_to_float64(rawdata)
elif dtype == "npy":
    rawdata = np.load(recorded_filepath)
    data = norm_to_float64(rawdata)

if debug_plots:
    plt.plot(data)
    plt.xlabel("Samples")
    plt.title("Input data")
    plt.tight_layout()
    plt.show()

print("Synchronising via chirp ...")
chirp_conv = syncest.convolve_chirp_rev(data)

if debug_plots:
    plt.plot(chirp_conv)
    plt.title("Signal convolved with reversed chirp")
    plt.xlabel("Samples")
    plt.tight_layout()
    plt.show()

frame_index = syncest.chirp_synchronise(chirp_conv.copy())
print(f"Found {len(frame_index)-1} frames, at {frame_index}")

no_frames = len(frame_index) - 1
frame_real_lengths = np.zeros(no_frames)

for frame in range(no_frames):
    frame_start = frame_index[frame]
    frame_end = frame_index[frame+1]
    sample_len = (frame_end) - (frame_start)
    time_len = sample_len / Fs

    sample_shift = (sample_len-(chirp_samp*2+known_OFDM_sample*2)) % B
    if sample_shift > B/2:
        sample_len_real = ((sample_len-chirp_samp*2-known_OFDM_sample*2) // B + 1) * B + chirp_samp*2 + known_OFDM_sample*2
        sample_shift = B - sample_shift
    else:
        sample_len_real = ((sample_len-chirp_samp*2-known_OFDM_sample*2) // B) * B + chirp_samp*2 + known_OFDM_sample*2
    time_len_real = sample_len_real / Fs
    print(f"Frame {frame+1}. Data length: {sample_len} samples, {time_len} seconds")
    print(f"Shifted by {sample_shift} samples")
    print(f"Real known_ofdm_symbols length: {sample_len_real} samples, {time_len_real} seconds")
    frame_real_lengths[frame] = sample_len_real

if debug_plots:
    real_len_cum = np.cumsum(np.concatenate([np.zeros(1), frame_real_lengths])) + frame_index[0]
    for i, f in enumerate(frame_index):
        plt.plot(chirp_conv[f-1000:f+2000], label=f"chirp {i}")
    plt.xlabel("Samples")
    plt.ylabel("Amplitude")
    plt.legend()
    plt.title("Chirp convolved impulse responses with sync point shifted to 1000")
    plt.tight_layout()
    plt.show()


def frame_processing(data, frame_index, no_frames, frame_real_lengths):
    all_llrs = []
    received_const_values = []
    for frame in range(no_frames):
        frame_data = data[frame_index[frame]:frame_index[frame+1]]
        frame_data = sig.resample(frame_data, int(frame_real_lengths[frame]))

        (channel_freq_resp_front, channel_freq_resp_end), (real_var_est, imag_var_est), _ = syncest.estimate_channel_known_ofdm(frame_data, 0)
        channel_freq_resp = (channel_freq_resp_front + channel_freq_resp_end)/2

        print(f"Estimated noise variance: {(real_var_est, imag_var_est)}")
        frame_data = frame_data[chirp_samp+known_OFDM_sample:-(chirp_samp+known_OFDM_sample)]
        signal_blocks = []

        for i in range(int(frame_data.shape[0]/B)):
            signal_blocks.append(frame_data[i * B + L: (i + 1) * B])

        for b_num, block in enumerate(signal_blocks):
            block_fft = np.fft.fft(block)
            block_fft = block_fft[min_freq_bin:max_freq_bin]
            gain_norm_block = block_fft/channel_freq_resp[min_freq_bin:max_freq_bin]

            if show_plots: received_const_values.append(gain_norm_block)

            const_mag = 1
            if method != "known_ofdm":
                # The sinc or white noise methods do not give accurate magnitudes
                const_mag = syncest.estimate_constellation_magnitude(gain_norm_block)
            bit_llrs = constellation.get_llrs(gain_norm_block/const_mag,
                                              real_var_est,
                                              imag_var_est)
            all_llrs.append(bit_llrs)
    return all_llrs, received_const_values


print("Starting resampling, estimation and soft decoding ...")
all_llrs, received_const_values = frame_processing(data, frame_index, no_frames, frame_real_lengths)
all_llrs = np.concatenate(all_llrs)

if show_plots:
    # Show overall constellation
    subcarrier_index = np.tile(np.arange(min_freq_bin+1, max_freq_bin), len(received_const_values))
    received_const_values = np.concatenate([r[1:] for r in received_const_values])

    plt.scatter(np.imag(received_const_values),
            np.real(received_const_values),
            c=subcarrier_index, cmap="viridis",
            s=1)
    plt.xlabel("I")
    plt.ylabel("Q")
    plt.title("Constellation plot")
    plt.colorbar(label="Subcarrier index")
    plt.tight_layout()
    plt.show()


def ldpc_decode(llrs):
    code_len = ldpc_module.N
    num_code_blocks = llrs.shape[0]//code_len
    post_llrs = []

    for i in range(num_code_blocks):
        block_llr = llrs[i*code_len:(i+1)*code_len]
        post_llr, it, = ldpc_module.decode(block_llr, ldpc_decode_mode)
        print(f"Code block: {i}, LDPC iterations used : {it}\r", end='')
        if it >= 200:
            print("\nWarning: LDPC decode reached max iterations")
        post_llrs.append(post_llr[:int(len(post_llr)*ldpc_rate)])

    return np.concatenate(post_llrs)


if coded_with_ldpc:
    print("Performing LDPC decoding ...")
    # Update with posterior llrs
    all_llrs = ldpc_decode(all_llrs)

decoded_bits = (all_llrs <= 0).astype(np.uint8)
file_data = np.packbits(decoded_bits, bitorder="big")


def decode_header(file_bytearray):
    try:
        file_len = int.from_bytes(file_bytearray[:4], "little")
        print(file_len)
        file_name_bytes, file_bytes = file_bytearray[4:].split(b'\0', 1)
        print(f"Header length {4 + len(file_name_bytes)}")
        file_name = file_name_bytes.decode('ascii')
        file_data = file_bytes[:file_len]
    except (UnicodeDecodeError, IndexError, ValueError):
        print("Header corrupted")
        return None, None

    if len(file_name) == 0:
        print("Header corrupted")
        return None, None

    # take just the filename if the transmitted name is really a path
    file_name = file_name.split("/")[-1]

    return file_name, file_data


# Write file
file_bytearray = bytearray(file_data)

filename = None
file_data = None

if data_has_header:
    filename, file_data = decode_header(file_bytearray)
if filename is None or file_data is None:
    # either no header provided, or cannot decode header
    filename = f"{recorded_filepath.split('/')[-1][:-4]}.txt"
    file_data = file_bytearray

print(f"file name: {filename}")
print(f"file length: {len(file_data)} bytes")
print("Writing output ...")

with open(f"./decoded_files/{filename}", 'wb') as f:
    f.write(bytes(file_data))
    f.close()

print("Complete")

